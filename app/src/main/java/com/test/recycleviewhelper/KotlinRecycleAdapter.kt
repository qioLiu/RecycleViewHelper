package com.kotlin.kotlin.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.kotlin.kotlin.mode.KotlinTestBean
import com.test.recycleviewhelper.R
import kotlinx.android.synthetic.main.recycle_item.view.*

/**
 * kotlin语法的适配器
 * Created by six.sev on 2017/6/21.
 */

class KotlinRecycleAdapter(var context: Context, var data: List<KotlinTestBean>) : RecyclerView.Adapter<KotlinRecycleAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): MyViewHolder {
        return MyViewHolder(LayoutInflater.from(context).inflate(R.layout.recycle_item, null))
    }

    override fun onBindViewHolder(holder: MyViewHolder?, position: Int) {
        /**
         * ?注解表示该变量可以为空，当为空的时候返回null。
         * ？.表示当？前面的不为空的时候才执行后面的， 当为空直接返回null
         * holder?:XXX  表示当holder为空的时候才执行?:后面的部分
         */

        holder!!.bind(data[position])
    }

    override fun getItemCount() = data?.size ?: 0

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        fun bind(bean: KotlinTestBean?) = with(itemView){
            /**
             * !!符号表示确认该变量一定不为空，如果使用则会跑出nullpotionerException
             */
            name!!.text = bean?.name
            age!!.text = bean?.age.toString()
        }
    }

}


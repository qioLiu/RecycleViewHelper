package com.kotlin.kotlin.mode

/**
 * Created by MTJF-0509 on 2017/6/21.
 */

data class KotlinTestBean(val name : String, val age : Int)

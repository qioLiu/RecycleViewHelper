package com.test.recycleviewhelper

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.Toast
import com.kotlin.kotlin.adapter.KotlinRecycleAdapter
import com.kotlin.kotlin.mode.KotlinTestBean
import kotlinx.android.synthetic.main.activity_main.*;

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        recycler_view.layoutManager = LinearLayoutManager(this)
        recycler_view.adapter = KotlinRecycleAdapter(this, listOf(KotlinTestBean("wo", 4), KotlinTestBean("wo", 4), KotlinTestBean("wo", 4), KotlinTestBean("wo", 4), KotlinTestBean("wo", 4), KotlinTestBean("wo", 4)))
        recycler_view.addOnItemTouchListener(object: RecyclerViewItemTouchListener(recycler_view) {
            override fun onItemClick(holder: RecyclerView.ViewHolder) {
                Toast.makeText(baseContext, "onitemclick", Toast.LENGTH_SHORT).show()
            }

            override fun onItemLongClick(holder: RecyclerView.ViewHolder) {
                Toast.makeText(baseContext, "onItemLongClick", Toast.LENGTH_SHORT).show()
            }
        })
    }
}

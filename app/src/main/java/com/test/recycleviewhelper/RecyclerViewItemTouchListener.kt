package com.test.recycleviewhelper

import android.support.v4.view.GestureDetectorCompat
import android.support.v7.widget.RecyclerView
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View

/**
 * DESCRIPTION
 * com.test.recycleviewhelper
 * Created by six.sev on 2017/9/15.
 */
abstract class RecyclerViewItemTouchListener(val mRecyclerView: RecyclerView) : RecyclerView.OnItemTouchListener{
    var gestureDetectorCompat: GestureDetectorCompat? = null
    init {
        gestureDetectorCompat = GestureDetectorCompat(mRecyclerView.context, object: RecyclerOnGestureListener(){})
    }

    override fun onInterceptTouchEvent(rv: RecyclerView?, e: MotionEvent?): Boolean {
        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        gestureDetectorCompat!!.onTouchEvent(e)
        return false
    }

    override fun onTouchEvent(rv: RecyclerView?, e: MotionEvent?) {
        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        gestureDetectorCompat!!.onTouchEvent(e)
    }

    override fun onRequestDisallowInterceptTouchEvent(disallowIntercept: Boolean) {
        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    open inner class RecyclerOnGestureListener : GestureDetector.SimpleOnGestureListener() {

        override fun onSingleTapUp(e: MotionEvent?): Boolean {
            //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            val findChildViewUnder: View? = mRecyclerView.findChildViewUnder(e!!.x, e!!.y)
            val childViewHolder: RecyclerView.ViewHolder? = mRecyclerView.getChildViewHolder(findChildViewUnder!!)
            onItemClick(childViewHolder!!)
            return true
        }

        override fun onLongPress(e: MotionEvent?) {
            //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            val findChildViewUnder: View? = mRecyclerView.findChildViewUnder(e!!.x, e!!.y)
            val childViewHolder: RecyclerView.ViewHolder? = mRecyclerView.getChildViewHolder(findChildViewUnder!!)
            onItemLongClick(childViewHolder!!)
        }
    }

    abstract fun onItemClick(holder: RecyclerView.ViewHolder)
    abstract fun onItemLongClick(holder: RecyclerView.ViewHolder)
}